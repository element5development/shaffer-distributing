<?php 
/*----------------------------------------------------------------*\

	SINGLE PRODUCT TEMPLATE

\*----------------------------------------------------------------*/
?>


<?php get_header(); ?>

<?php get_template_part('template-parts/elements/navigation'); ?>

<?php get_template_part('template-parts/elements/navigation-mobile'); ?>

<header class="page-title">
	<section>
		<?php $terms = get_the_terms( $post->ID, 'productcategory' ); ?>
		<?php $term = array_pop($terms); ?>
		<?php echo '<p><a class="button" href="'.get_term_link($term->slug, 'productcategory').'">Back to '.$term->name.'</a></p>' ?>
	</section>
</header>
<main>
	<article>
		<div class="product-gallery-wrap">
			<?php $gallery = get_field('product_images'); ?>
			<?php $singleimage = get_field('product_image'); ?>
			<?php if( $gallery ): ?>
				<div class="product-gallery">
					<?php foreach( $gallery as $galleryimage ): ?>
						<img src="<?php echo esc_url($galleryimage['url']); ?>" alt="<?php echo esc_attr($galleryimage['alt']); ?>" />
					<?php endforeach; ?>
				</div>
				<div class="thumbnails">
					<?php foreach( $gallery as $galleryimage ): ?>
						<img src="<?php echo esc_url($galleryimage['sizes']['small']); ?>" alt="Thumbnail of <?php echo esc_url($galleryimage['alt']); ?>" />
					<?php endforeach; ?>
				</div>
			<?php elseif ( $singleimage ): ?>
				<div class="product-gallery">
					<img src="<?php echo esc_url($singleimage['url']); ?>" alt="<?php echo esc_attr($singleimage['alt']); ?>" />
				</div>
			<?php endif; ?>
		</div>
		<div class="product-info">
			<?php echo get_the_term_list( $post->ID, 'vendor', '', '', '' ) ?>
			<h1><?php the_title(); ?></h1>
			<h2><?php the_field('price'); ?></h2>
			<?php if ( get_field('product_short_description') ) : ?>
				<?php the_field('product_short_description'); ?>
			<?php endif; ?>
			<div class="buttons">
				<a class="button" href="<?php echo get_site_url(); ?>/contact-for-pricing/?product_name=<?php the_title(); ?>">Contact For Pricing</a>
				<?php $file = get_field('product_sales_sheet'); ?>
				<?php if( $file ): ?>
					<a class="button is-borderless has-icon" href="<?php echo $file; ?>" target="_blank">Product Sales Sheet</a>
				<?php endif; ?>
			</div>
			<div class="specifications">
				<h4>Specifications</h4>
				<?php if( have_rows('specifications') ): ?>
					<ul>
						<?php while ( have_rows('specifications') ) : the_row(); ?>
							<li><?php the_sub_field('specification'); ?></li>
						<?php endwhile; ?>
					</ul>
				<?php endif; ?>
			</div>
		</div>
		<div class="product-description">
			<h3>Product Description</h3>
			<?php if ( get_field('product_description') ) : ?>
				<?php the_field('product_description'); ?>
			<?php endif; ?>
		</div>
	</article>
</main>

<?php get_template_part('template-parts/sections/product-reviews'); ?>

<?php get_template_part('template-parts/sections/related-products'); ?>

<?php get_template_part('template-parts/sections/footers/footer'); ?>

<?php get_footer(); ?>