<?php 
/*----------------------------------------------------------------*\

	Template Name: Contact
	
\*----------------------------------------------------------------*/
?>

<?php get_header(); ?>

<?php get_template_part('template-parts/elements/navigation'); ?>

<?php get_template_part('template-parts/elements/navigation-mobile'); ?>

<?php get_template_part('template-parts/sections/headers/header-contact'); ?>

<main>
	<article>
		<?php if ( '' !== get_post()->post_content ) : ?>
			<section class="main-content-block">
				<?php the_content(); ?>
			</section>
		<?php endif; ?>
	</article>
</main>

<?php get_template_part('template-parts/sections/locations'); ?>

<?php get_template_part('template-parts/sections/footers/footer'); ?>

<?php get_footer(); ?>