<?php 
/*----------------------------------------------------------------*\

	Template Name: Thank You
	
\*----------------------------------------------------------------*/
?>

<?php get_header(); ?>

<?php get_template_part('template-parts/elements/navigation'); ?>

<?php get_template_part('template-parts/elements/navigation-mobile'); ?>

<main>
	<article>
		<section class="wysiwyg-block">
			<h1><?php the_title(); ?></h1>
			<p><?php the_field('title_description'); ?></p>
			<div class="buttons">
				<a class="button is-primary" href="/">Return to the Homepage</a>
				<a class="button is-ghost is-white" href="/contact-us/">Contact Support</a>
			</div>
		</section>
	</article>
</main>

<?php get_template_part('template-parts/sections/footers/footer'); ?>

<?php get_footer(); ?>