<?php

/*----------------------------------------------------------------*\

	CUSTOM POST TYPES
	www.wp-hasty.com

\*----------------------------------------------------------------*/
// Register Custom Post Type Product
// Post Type Key: product
function create_product_cpt() {

	$labels = array(
		'name' => __( 'Products', 'Post Type General Name', 'textdomain' ),
		'singular_name' => __( 'Product', 'Post Type Singular Name', 'textdomain' ),
		'menu_name' => __( 'Products', 'textdomain' ),
		'name_admin_bar' => __( 'Product', 'textdomain' ),
		'archives' => __( 'Product Archives', 'textdomain' ),
		'attributes' => __( 'Product Attributes', 'textdomain' ),
		'parent_item_colon' => __( 'Parent Product:', 'textdomain' ),
		'all_items' => __( 'All Products', 'textdomain' ),
		'add_new_item' => __( 'Add New Product', 'textdomain' ),
		'add_new' => __( 'Add New', 'textdomain' ),
		'new_item' => __( 'New Product', 'textdomain' ),
		'edit_item' => __( 'Edit Product', 'textdomain' ),
		'update_item' => __( 'Update Product', 'textdomain' ),
		'view_item' => __( 'View Product', 'textdomain' ),
		'view_items' => __( 'View Products', 'textdomain' ),
		'search_items' => __( 'Search Product', 'textdomain' ),
		'not_found' => __( 'Not found', 'textdomain' ),
		'not_found_in_trash' => __( 'Not found in Trash', 'textdomain' ),
		'featured_image' => __( 'Featured Image', 'textdomain' ),
		'set_featured_image' => __( 'Set featured image', 'textdomain' ),
		'remove_featured_image' => __( 'Remove featured image', 'textdomain' ),
		'use_featured_image' => __( 'Use as featured image', 'textdomain' ),
		'insert_into_item' => __( 'Insert into Product', 'textdomain' ),
		'uploaded_to_this_item' => __( 'Uploaded to this Product', 'textdomain' ),
		'items_list' => __( 'Products list', 'textdomain' ),
		'items_list_navigation' => __( 'Products list navigation', 'textdomain' ),
		'filter_items_list' => __( 'Filter Products list', 'textdomain' ),
	);
	$args = array(
		'label' => __( 'Product', 'textdomain' ),
		'description' => __( '', 'textdomain' ),
		'labels' => $labels,
		'menu_icon' => 'dashicons-products',
		'supports' => array('title', 'editor', 'page-attributes', 'post-formats', 'custom-fields', ),
		'taxonomies' => array('Vendor', 'Product Category' ),
		'public' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'menu_position' => 5,
		'show_in_admin_bar' => true,
		'show_in_nav_menus' => true,
		'can_export' => true,
		'has_archive' => true,
		"rewrite" => array('slug' => 'products'),
		'hierarchical' => false,
		'exclude_from_search' => false,
		'show_in_rest' => true,
		'publicly_queryable' => true,
		'capability_type' => 'post',
	);
	register_post_type( 'product', $args );

}
add_action( 'init', 'create_product_cpt', 0 );
// Register Custom Post Type Team
// Post Type Key: team
function create_team_cpt() {

	$labels = array(
		'name' => __( 'Team', 'Post Type General Name', 'textdomain' ),
		'singular_name' => __( 'Team', 'Post Type Singular Name', 'textdomain' ),
		'menu_name' => __( 'Team', 'textdomain' ),
		'name_admin_bar' => __( 'Team', 'textdomain' ),
		'archives' => __( 'Team Archives', 'textdomain' ),
		'attributes' => __( 'Team Attributes', 'textdomain' ),
		'parent_item_colon' => __( 'Parent Team:', 'textdomain' ),
		'all_items' => __( 'All Team', 'textdomain' ),
		'add_new_item' => __( 'Add New Team', 'textdomain' ),
		'add_new' => __( 'Add New', 'textdomain' ),
		'new_item' => __( 'New Team', 'textdomain' ),
		'edit_item' => __( 'Edit Team', 'textdomain' ),
		'update_item' => __( 'Update Team', 'textdomain' ),
		'view_item' => __( 'View Team', 'textdomain' ),
		'view_items' => __( 'View Team', 'textdomain' ),
		'search_items' => __( 'Search Team', 'textdomain' ),
		'not_found' => __( 'Not found', 'textdomain' ),
		'not_found_in_trash' => __( 'Not found in Trash', 'textdomain' ),
		'featured_image' => __( 'Featured Image', 'textdomain' ),
		'set_featured_image' => __( 'Set featured image', 'textdomain' ),
		'remove_featured_image' => __( 'Remove featured image', 'textdomain' ),
		'use_featured_image' => __( 'Use as featured image', 'textdomain' ),
		'insert_into_item' => __( 'Insert into Team', 'textdomain' ),
		'uploaded_to_this_item' => __( 'Uploaded to this Team', 'textdomain' ),
		'items_list' => __( 'Team list', 'textdomain' ),
		'items_list_navigation' => __( 'Team list navigation', 'textdomain' ),
		'filter_items_list' => __( 'Filter Team list', 'textdomain' ),
	);
	$args = array(
		'label' => __( 'Team', 'textdomain' ),
		'description' => __( '', 'textdomain' ),
		'labels' => $labels,
		'menu_icon' => 'dashicons-admin-users',
		'supports' => array('title', 'page-attributes', 'post-formats', 'custom-fields', ),
		'taxonomies' => array('Division', ),
		'public' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'menu_position' => 5,
		'show_in_admin_bar' => true,
		'show_in_nav_menus' => true,
		'can_export' => true,
		'has_archive' => false,
		'hierarchical' => false,
		'exclude_from_search' => false,
		'show_in_rest' => true,
		'publicly_queryable' => false,
		'capability_type' => 'post',
	);
	register_post_type( 'team', $args );

}
add_action( 'init', 'create_team_cpt', 0 );
// Register Custom Post Type Testimonial
// Post Type Key: testimonial
function create_testimonial_cpt() {

	$labels = array(
		'name' => __( 'Testimonials', 'Post Type General Name', 'textdomain' ),
		'singular_name' => __( 'Testimonial', 'Post Type Singular Name', 'textdomain' ),
		'menu_name' => __( 'Testimonials', 'textdomain' ),
		'name_admin_bar' => __( 'Testimonial', 'textdomain' ),
		'archives' => __( 'Testimonial Archives', 'textdomain' ),
		'attributes' => __( 'Testimonial Attributes', 'textdomain' ),
		'parent_item_colon' => __( 'Parent Testimonial:', 'textdomain' ),
		'all_items' => __( 'All Testimonials', 'textdomain' ),
		'add_new_item' => __( 'Add New Testimonial', 'textdomain' ),
		'add_new' => __( 'Add New', 'textdomain' ),
		'new_item' => __( 'New Testimonial', 'textdomain' ),
		'edit_item' => __( 'Edit Testimonial', 'textdomain' ),
		'update_item' => __( 'Update Testimonial', 'textdomain' ),
		'view_item' => __( 'View Testimonial', 'textdomain' ),
		'view_items' => __( 'View Testimonials', 'textdomain' ),
		'search_items' => __( 'Search Testimonial', 'textdomain' ),
		'not_found' => __( 'Not found', 'textdomain' ),
		'not_found_in_trash' => __( 'Not found in Trash', 'textdomain' ),
		'featured_image' => __( 'Featured Image', 'textdomain' ),
		'set_featured_image' => __( 'Set featured image', 'textdomain' ),
		'remove_featured_image' => __( 'Remove featured image', 'textdomain' ),
		'use_featured_image' => __( 'Use as featured image', 'textdomain' ),
		'insert_into_item' => __( 'Insert into Testimonial', 'textdomain' ),
		'uploaded_to_this_item' => __( 'Uploaded to this Testimonial', 'textdomain' ),
		'items_list' => __( 'Testimonials list', 'textdomain' ),
		'items_list_navigation' => __( 'Testimonials list navigation', 'textdomain' ),
		'filter_items_list' => __( 'Filter Testimonials list', 'textdomain' ),
	);
	$args = array(
		'label' => __( 'Testimonial', 'textdomain' ),
		'description' => __( '', 'textdomain' ),
		'labels' => $labels,
		'menu_icon' => 'dashicons-testimonial',
		'supports' => array('title', 'custom-fields', ),
		'taxonomies' => array(),
		'public' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'menu_position' => 5,
		'show_in_admin_bar' => true,
		'show_in_nav_menus' => true,
		'can_export' => true,
		'has_archive' => false,
		'hierarchical' => false,
		'exclude_from_search' => false,
		'show_in_rest' => true,
		'publicly_queryable' => false,
		'capability_type' => 'post',
	);
	register_post_type( 'testimonial', $args );

}
add_action( 'init', 'create_testimonial_cpt', 0 );
// Register Custom Post Type Vendor
// Post Type Key: vendor
function create_vendor_cpt() {

	$labels = array(
		'name' => __( 'Vendors', 'Post Type General Name', 'textdomain' ),
		'singular_name' => __( 'Vendor', 'Post Type Singular Name', 'textdomain' ),
		'menu_name' => __( 'Vendors', 'textdomain' ),
		'name_admin_bar' => __( 'Vendor', 'textdomain' ),
		'archives' => __( 'Vendor Archives', 'textdomain' ),
		'attributes' => __( 'Vendor Attributes', 'textdomain' ),
		'parent_item_colon' => __( 'Parent Vendor:', 'textdomain' ),
		'all_items' => __( 'All Vendors', 'textdomain' ),
		'add_new_item' => __( 'Add New Vendor', 'textdomain' ),
		'add_new' => __( 'Add New', 'textdomain' ),
		'new_item' => __( 'New Vendor', 'textdomain' ),
		'edit_item' => __( 'Edit Vendor', 'textdomain' ),
		'update_item' => __( 'Update Vendor', 'textdomain' ),
		'view_item' => __( 'View Vendor', 'textdomain' ),
		'view_items' => __( 'View Vendors', 'textdomain' ),
		'search_items' => __( 'Search Vendor', 'textdomain' ),
		'not_found' => __( 'Not found', 'textdomain' ),
		'not_found_in_trash' => __( 'Not found in Trash', 'textdomain' ),
		'featured_image' => __( 'Featured Image', 'textdomain' ),
		'set_featured_image' => __( 'Set featured image', 'textdomain' ),
		'remove_featured_image' => __( 'Remove featured image', 'textdomain' ),
		'use_featured_image' => __( 'Use as featured image', 'textdomain' ),
		'insert_into_item' => __( 'Insert into Vendor', 'textdomain' ),
		'uploaded_to_this_item' => __( 'Uploaded to this Vendor', 'textdomain' ),
		'items_list' => __( 'Vendors list', 'textdomain' ),
		'items_list_navigation' => __( 'Vendors list navigation', 'textdomain' ),
		'filter_items_list' => __( 'Filter Vendors list', 'textdomain' ),
	);
	$args = array(
		'label' => __( 'Vendor', 'textdomain' ),
		'description' => __( '', 'textdomain' ),
		'labels' => $labels,
		'menu_icon' => 'dashicons-tag',
		'supports' => array('title', 'custom-fields', ),
		'taxonomies' => array(),
		'public' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'menu_position' => 5,
		'show_in_admin_bar' => true,
		'show_in_nav_menus' => true,
		'can_export' => true,
		'has_archive' => false,
		'hierarchical' => false,
		'exclude_from_search' => false,
		'show_in_rest' => true,
		'publicly_queryable' => false,
		'capability_type' => 'post',
	);
	register_post_type( 'vendor', $args );

}
add_action( 'init', 'create_vendor_cpt', 0 );