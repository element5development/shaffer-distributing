<?php

/*----------------------------------------------------------------*\

	CUSTOM TAXONOMIES
	www.wp-hasty.com

\*----------------------------------------------------------------*/
// Register Taxonomy Vendor
// Taxonomy Key: vendor
function create_vendor_tax() {

	$labels = array(
		'name'              => _x( 'Vendors', 'taxonomy general name', 'textdomain' ),
		'singular_name'     => _x( 'Vendor', 'taxonomy singular name', 'textdomain' ),
		'search_items'      => __( 'Search Vendors', 'textdomain' ),
		'all_items'         => __( 'All Vendors', 'textdomain' ),
		'parent_item'       => __( 'Parent Vendor', 'textdomain' ),
		'parent_item_colon' => __( 'Parent Vendor:', 'textdomain' ),
		'edit_item'         => __( 'Edit Vendor', 'textdomain' ),
		'update_item'       => __( 'Update Vendor', 'textdomain' ),
		'add_new_item'      => __( 'Add New Vendor', 'textdomain' ),
		'new_item_name'     => __( 'New Vendor Name', 'textdomain' ),
		'menu_name'         => __( 'Vendor', 'textdomain' ),
	);
	$args = array(
		'labels' => $labels,
		'description' => __( '', 'textdomain' ),
		'hierarchical' => false,
		'public' => true,
		'publicly_queryable' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'show_in_nav_menus' => true,
		'show_in_rest' => false,
		'show_tagcloud' => true,
		'show_in_quick_edit' => true,
		'show_admin_column' => false,
	);
	register_taxonomy( 'vendor', array('product', ), $args );

}
add_action( 'init', 'create_vendor_tax' );
// Register Taxonomy Product Category
// Taxonomy Key: productcategory
function create_productcategory_tax() {

	$labels = array(
		'name'              => _x( 'Product Categories', 'taxonomy general name', 'textdomain' ),
		'singular_name'     => _x( 'Product Category', 'taxonomy singular name', 'textdomain' ),
		'search_items'      => __( 'Search Product Categories', 'textdomain' ),
		'all_items'         => __( 'All Product Categories', 'textdomain' ),
		'parent_item'       => __( 'Parent Product Category', 'textdomain' ),
		'parent_item_colon' => __( 'Parent Product Category:', 'textdomain' ),
		'edit_item'         => __( 'Edit Product Category', 'textdomain' ),
		'update_item'       => __( 'Update Product Category', 'textdomain' ),
		'add_new_item'      => __( 'Add New Product Category', 'textdomain' ),
		'new_item_name'     => __( 'New Product Category Name', 'textdomain' ),
		'menu_name'         => __( 'Product Category', 'textdomain' ),
	);
	$args = array(
		'labels' => $labels,
		'description' => __( '', 'textdomain' ),
		'hierarchical' => true,
		'public' => true,
		'publicly_queryable' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'show_in_nav_menus' => true,
		'show_in_rest' => false,
		'show_tagcloud' => true,
		'show_in_quick_edit' => true,
		'show_admin_column' => false,
		"rewrite" => array('slug' => 'product-category'),
	);
	register_taxonomy( 'productcategory', array('product', ), $args );

}
add_action( 'init', 'create_productcategory_tax' );
// Register Taxonomy Division
// Taxonomy Key: division
function create_division_tax() {

	$labels = array(
		'name'              => _x( 'Division', 'taxonomy general name', 'textdomain' ),
		'singular_name'     => _x( 'Division', 'taxonomy singular name', 'textdomain' ),
		'search_items'      => __( 'Search Division', 'textdomain' ),
		'all_items'         => __( 'All Division', 'textdomain' ),
		'parent_item'       => __( 'Parent Division', 'textdomain' ),
		'parent_item_colon' => __( 'Parent Division:', 'textdomain' ),
		'edit_item'         => __( 'Edit Division', 'textdomain' ),
		'update_item'       => __( 'Update Division', 'textdomain' ),
		'add_new_item'      => __( 'Add New Division', 'textdomain' ),
		'new_item_name'     => __( 'New Division Name', 'textdomain' ),
		'menu_name'         => __( 'Division', 'textdomain' ),
	);
	$args = array(
		'labels' => $labels,
		'description' => __( '', 'textdomain' ),
		'hierarchical' => false,
		'public' => true,
		'publicly_queryable' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'show_in_nav_menus' => true,
		'show_in_rest' => false,
		'show_tagcloud' => true,
		'show_in_quick_edit' => true,
		'show_admin_column' => false,
	);
	register_taxonomy( 'division', array('team', ), $args );

}
add_action( 'init', 'create_division_tax' );