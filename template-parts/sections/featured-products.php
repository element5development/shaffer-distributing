<?php 
/*----------------------------------------------------------------*\

	FEATURED PRODUCTS SECTION

\*----------------------------------------------------------------*/
?>

<?php $featured_posts = get_field('products'); ?>
<?php if( $featured_posts ): ?>
<section class="related-products">
	<h2><?php the_field('product_section_title'); ?></h2>
	<?php foreach( $featured_posts as $post ): ?>
		<?php setup_postdata($post); ?>

		<article class="preview preview-post preview-product">
			<a href="<?php the_permalink(); ?>"></a>
			<div class="featured-image">
				<?php $image = get_field('product_image'); ?>
				<img src="<?php echo $image['sizes']['small']; ?>" alt="<?php echo $image['alt']; ?>" />
			</div>
			<h4><?php the_field('price'); ?></h4>
			<h2><?php the_title(); ?></h2>
			<div class="buttons">
				<div class="button is-ghost">View Details</div>
			</div>
		</article>

	<?php endforeach; ?>
	<a class="button" href="<?php echo get_site_url(); ?>/products/">See All Products</a>
</section>
<?php wp_reset_postdata(); ?>
<?php endif; ?>