<?php 
/*----------------------------------------------------------------*\

	LEADERSHIP SECTION

\*----------------------------------------------------------------*/
?>

<?php $term = get_field('division'); ?>
<?php if( $term ): ?>
<section class="leadership">
	<div>
		<h2><?php echo $term->name; ?></h2>
		<?php
			$args = array(
				'post_type' => 'team',
				'posts_per_page' => -1,
				'tax_query' => array(
					array (
							'taxonomy' => 'division',
							'field' => 'id',
							'terms' => $term->term_id,
					)
				),
			);
			$loop = new WP_Query( $args );
		?>
		<?php if ( $loop->have_posts() ) : ?>
			<?php while ( $loop->have_posts() ) : $loop->the_post();?>
				<?php get_template_part( 'template-parts/elements/previews/preview-team' ); ?>
			<?php endwhile; ?>
		<?php endif; ?>
		<?php wp_reset_query(); ?>
		<div>
			<a class="button" href="<?php echo get_site_url(); ?>/about-us/meet-our-team/">Meet the Team</a>
		</div>
	</div>
</section>
<?php endif; ?>