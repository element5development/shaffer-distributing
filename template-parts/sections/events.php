<?php 
/*----------------------------------------------------------------*\

	EVENTS SECTION

\*----------------------------------------------------------------*/
?>

<?php if( have_rows('event_repeater') ): ?>
<section class="events">
	<h2><?php the_field('event_section_title'); ?></h2>
	<?php while ( have_rows('event_repeater') ) : the_row(); ?>
		<?php get_template_part( 'template-parts/elements/previews/preview-event' ); ?>
	<?php endwhile; ?>
</section>
<?php endif; ?>