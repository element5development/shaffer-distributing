<?php 
/*----------------------------------------------------------------*\

	MEMBER CARD SECTION

\*----------------------------------------------------------------*/
?>

<?php if( have_rows('member_card_repeater') ): ?>
<section class="member-cards">
	<?php while ( have_rows('member_card_repeater') ) : the_row(); ?>
		<?php get_template_part( 'template-parts/elements/previews/preview-member-card' ); ?>
	<?php endwhile; ?>
</section>
<?php endif; ?>