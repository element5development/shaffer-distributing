<?php 
/*----------------------------------------------------------------*\

	JOB OPENINGS SECTION

\*----------------------------------------------------------------*/
?>

<?php if( have_rows('opening_repeater') ): ?>
<section class="openings">
	<h2><?php the_field('section_title'); ?></h2>
	<?php while ( have_rows('opening_repeater') ) : the_row(); ?>
		<?php get_template_part( 'template-parts/elements/previews/preview-opening' ); ?>
	<?php endwhile; ?>
</section>
<?php endif; ?>