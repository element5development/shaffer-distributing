<?php 
/*----------------------------------------------------------------*\

	TEAM SECTION

\*----------------------------------------------------------------*/
?>

<?php if( have_rows('division_repeater') ): ?>
<div id="all"></div>
<section class="team">
	<div>
		<h4>Sort by:</h4>
		<a class="sort-by is-active" href="#all">All</a>
		<?php while ( have_rows('division_repeater') ) : the_row(); ?>

			<?php $term = get_sub_field('divison'); ?>
			<?php if( $term ): ?>
				<a class="sort-by" href="#<?php echo $term->name; ?>"><?php echo $term->name; ?></a>
			<?php endif; ?>

		<?php endwhile; ?>
	</div>
	<div>
		<?php while ( have_rows('division_repeater') ) : the_row(); ?>
		
			<?php $term = get_sub_field('divison'); ?>
			<?php if( $term ): ?>
				<div>
					<div id="<?php echo $term->name; ?>"></div>
					<h2><?php echo $term->name; ?></h2>
					<?php
						$args = array(
							'post_type' => 'team',
							'posts_per_page' => -1,
							'tax_query' => array(
								array (
										'taxonomy' => 'division',
										'field' => 'id',
										'terms' => $term->term_id,
								)
							),
						);
						$loop = new WP_Query( $args );
					?>
					<?php if ( $loop->have_posts() ) : ?>
						<?php while ( $loop->have_posts() ) : $loop->the_post();?>
							<?php get_template_part( 'template-parts/elements/previews/preview-team' ); ?>
						<?php endwhile; ?>
					<?php endif; ?>
					<?php wp_reset_query(); ?>
				</div>
			<?php endif; ?>

		<?php endwhile; ?>
	</div>
</section>
<?php endif; ?>