<?php 
/*----------------------------------------------------------------*\

	HEADER WITH SLIDER

\*----------------------------------------------------------------*/
?>

<header class="page-title has-image home-slider">
	<?php if( have_rows('slide_repeater') ): ?>
		<?php while ( have_rows('slide_repeater') ) : the_row(); ?>
			<section style="background-image: linear-gradient(rgba(0, 0, 0, 0.45), rgba(0, 0, 0, 0.45)), url('<?php the_sub_field( 'slide_image'); ?>');">
				<div>
					<h1><?php the_sub_field('title'); ?></h1>
					<p><?php the_sub_field('description'); ?></p>
					<?php if( have_rows('button_repeater') ): ?>
						<?php while ( have_rows('button_repeater') ) : the_row(); ?>
							<?php $link = get_sub_field('button'); ?>
							<?php if( $link ): ?>
								<a class="button" href="<?php echo $link['url']; ?>" target="<?php echo $link['target']; ?>"><?php echo $link['title']; ?></a>
							<?php endif; ?>
						<?php endwhile; ?>
					<?php endif; ?>
				</div>
			</section>
		<?php endwhile; ?>
	<?php endif; ?>
</header>