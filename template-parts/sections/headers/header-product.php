<?php 
/*----------------------------------------------------------------*\

	SINGLE PRODUCT HEADER

\*----------------------------------------------------------------*/
?>

<header class="page-title">
	<section>
		<p><a href="<?php echo get_site_url(); ?>/products/">Products</a> / <span><?php the_title(); ?></span></p>
	</section>
</header>