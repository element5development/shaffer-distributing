<?php 
/*----------------------------------------------------------------*\

	HEADER FOR ALL ARCHIVES
	Used for any/all post types and search results

\*----------------------------------------------------------------*/
?>

<?php 
	//GET POST TYPE FROM PAGE
	//EMPTY FOR DEFAULT BLOG
	$posttype = get_query_var('post_type');
	//show category description
	$term_object = get_queried_object();
?>

<header class="page-title has-image is-taxonomy" style="background-image: url('<?php the_field( 'title_bg_img', 'option'); ?>');">
	<section>

		<h1><?php $tax = $wp_query->get_queried_object(); echo $tax->name; ?></h1>

		<?php if ( get_field( $posttype . '_title_description', 'option') ) : ?>
			<p class="subheader"><?php the_field( $posttype . '_title_description', 'option'); ?></p>
		<?php endif; ?>

		<?php if ( is_tax() && $term_object->description ) : ?>
			<p class="subheader"><?php echo $term_object->description; ?></p>
		<?php endif; ?>

	</section>

	<div class="overlay"></div>
</header>