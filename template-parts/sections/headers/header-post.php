<?php 
/*----------------------------------------------------------------*\

	DEFAULT HEADER
	Most basic and simple page title

\*----------------------------------------------------------------*/
?>

<?php $image = get_field('featured_img'); ?>
<header class="page-title has-image" style="background-image: url('<?php echo $image['sizes']['large']; ?>');">
	<section>
		<h1><?php if ( get_field('post_title') ) : the_field('post_title'); else : the_title(); endif; ?></h1>
		<div class="post-meta">
			<div>
				<p><?php echo get_the_date('M d, Y'); ?></p>
				<p><?php $categories = get_the_category($post->ID); echo $categories[0]->name; ?></p>
			</div>
		</div>
	</section>

	<div class="overlay"></div>

</header>