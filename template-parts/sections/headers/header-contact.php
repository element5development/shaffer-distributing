<?php 
/*----------------------------------------------------------------*\

	HEADER FOR CONTACT TEMPLATE

\*----------------------------------------------------------------*/
?>

<header class="page-title contact-title">
	<section>
		<div>
			<h1><?php the_title(); ?></h1>
			<?php echo do_shortcode('[gravityform id="'.get_field('form_id').'" title="false" description="false"]') ?>
		</div>
		<div>
			<?php the_field('google_map'); ?>
			<div>
				<h2><?php the_field('headquarters'); ?></h2>
				<h4><?php the_field('headquarters_location'); ?></h4>
				<p><?php the_field('headquarters_address'); ?><br><?php the_field('headquarters_city_state_zip'); ?></p>
				<?php if ( get_field('headquarters_phone') ) :?>
					<a class="button is-borderless has-icon" href="tel:+1<?php $remove = array(" ","(",")",".","-"); echo str_replace($remove, "", get_field('headquarters_phone')); ?>"><?php the_field('headquarters_phone'); ?></a>
				<?php endif; ?>
				<?php if ( get_field('headquarters_phone_local') ) :?>
					<a class="button is-borderless has-icon" href="tel:+1<?php $remove = array(" ","(",")",".","-"); echo str_replace($remove, "", get_field('headquarters_phone_local')); ?>"><?php the_field('headquarters_phone_local'); ?></a>
				<?php endif; ?>
				<?php if ( get_field('hours') ) :?>
					<p style="margin-top: 16px"><?php the_field('hours'); ?></p>
				<?php endif; ?>
		</div>
		</div>
	</section>
</header>