<?php 
/*----------------------------------------------------------------*\

	VENDORS SECTION

\*----------------------------------------------------------------*/
?>

<section class="vendors">
	<h2>Our Vendors</h2>
	<div class="vendor-logos">
	<?php // WP_Query arguments
		$args = array (
			'post_type'        => 'vendor',
			'post_status'      => 'publish',
			'posts_per_page' 	 => -1,
			'orderby' 				 => 'title',
			'order'   				 => 'ASC',
		);
		$vendors = new WP_Query( $args ); ?>

		<?php if ( $vendors->have_posts() ) : ?>
			<?php while ( $vendors->have_posts() ) : $vendors->the_post(); ?>
				<?php $image = get_field('vendor_logo'); ?>
				<img class="featured-img" src="<?php echo $image['sizes']['small']; ?>" alt="<?php echo $image['alt']; ?>" />
			<?php endwhile; ?>
		<?php else : ?>
			// no posts found
		<?php endif; ?>
		<?php wp_reset_postdata(); ?>
	</div>
</section>