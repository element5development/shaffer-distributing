<?php 
/*----------------------------------------------------------------*\

	ICON CARD SECTION

\*----------------------------------------------------------------*/
?>

<?php if( have_rows('card_repeater') ): ?>
<section class="icon-cards">
	<h2><?php the_field('section_title'); ?></h2>
	<?php while ( have_rows('card_repeater') ) : the_row(); ?>
		<?php get_template_part( 'template-parts/elements/previews/preview-card' ); ?>
	<?php endwhile; ?>
</section>
<?php endif; ?>