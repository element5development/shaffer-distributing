<?php 
/*----------------------------------------------------------------*\

	DISPLAY 0-3 RELATED PRODUCTS BY VENDOR

\*----------------------------------------------------------------*/
?>

<?php
	$terms = wp_get_object_terms( $post->ID, 'productcategory', array('fields' => 'ids') );
	$args = array(
		'post_type' => 'product',
		'posts_per_page' => 3,
		'post__not_in' => array( $post->ID ),
		'tax_query' => array(
			array (
					'taxonomy' => 'productcategory',
					'field' => 'id',
					'terms' => $terms,
			)
		),
	);
	$loop = new WP_Query( $args );
?>

<?php if ( $loop->have_posts() ) : ?>
<section class="related-products">
	<h2>Related Products</h2>
	<?php while ( $loop->have_posts() ) : $loop->the_post();?>
		<?php get_template_part( 'template-parts/elements/previews/preview-product' ); ?>
	<?php endwhile; ?>
</section>
<?php endif; ?>
<?php wp_reset_query(); ?>