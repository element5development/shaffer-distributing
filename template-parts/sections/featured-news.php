<?php 
/*----------------------------------------------------------------*\

	FEATURED NEWS SECTION

\*----------------------------------------------------------------*/
?>

<section class="featured-news">
	<div>
		<h2>News & Updates</h2>
		<div>
			<?php
			$args = array(
				'post_type' => 'post',
				'posts_per_page' => 2,
			);
			$loop = new WP_Query( $args );
			?>
			<?php while ( $loop->have_posts() ) : $loop->the_post();?>
				<?php get_template_part( 'template-parts/elements/previews/preview-blog' ); ?>
			<?php endwhile;?>
			<?php wp_reset_query(); ?>
		</div>
	</div>
	<div>
		<img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/whitepaper-icon.svg" alt="Whitepaper Icon"/>
		<h2>Download Our IAAPA Review 2019</h2>
		<p>Fill out the form below to download.</p>
		<?php echo do_shortcode('[gravityform id="5" title="false" description="false" ajax="true"]') ?>
	</div>
</section>