<?php 
/*----------------------------------------------------------------*\

	PRODUCT REVIEWS SECTION

\*----------------------------------------------------------------*/
?>
<?php if( have_rows('review_repeater') ): ?>
<section class="testimonials">
	<div class="block">
		<h2>Reviews from customers who bought this</h2>
		<div class="testimonial-slider">
				<?php while ( have_rows('review_repeater') ) : the_row(); ?>
					<article class="testimonial">
						<blockquote>
							<p>"<?php the_sub_field('review'); ?>"</p>
							<h4><?php the_sub_field('customer'); ?></h4>
							<p><?php the_sub_field('business'); ?></p>
						</blockquote>
					</article>
				<?php endwhile; ?>
		</div>
	</div>
</section>
<?php endif; ?>