<?php 
/*----------------------------------------------------------------*\

	DEFAULT FOOTER
	Made up of widget areas
	May need to add or remove additional areas depending on the design

\*----------------------------------------------------------------*/
?>

<footer>
	<div>
	
		<div class="newsletter">
			<h3>Subscribe to our newsletter</h3>
			<?php echo do_shortcode('[gravityform id="6" title="false" description="false" ajax=true]') ?>
		</div>

		<section>
			<nav>
				<?php wp_nav_menu(array( 'theme_location' => 'left_footer_nav' )); ?>
				<?php wp_nav_menu(array( 'theme_location' => 'right_footer_nav' )); ?>
			</nav>
		</section>

		<div class="company-info">
			<svg>
				<use xlink:href="#logo" />
			</svg>
			<div>
				<?php if ( get_field('phone_number', 'option') ) :?>
					<p>Toll Free Number</p>
					<a href="tel:+1<?php $remove = array(" ","(",")",".","-"); echo str_replace($remove, "", get_field('phone_number', 'option')); ?>"><?php the_field('phone_number', 'option'); ?></a>
				<?php endif; ?>
				<?php if ( get_field('phone_number_local', 'option') ) :?>
					<p>Local Number</p>
					<a href="tel:+1<?php $remove = array(" ","(",")",".","-"); echo str_replace($remove, "", get_field('phone_number_local', 'option')); ?>"><?php the_field('phone_number_local', 'option'); ?></a>
				<?php endif; ?>
				<?php if ( get_field('hours', 'option') ) :?>
					<p style="margin-top: 16px"><?php the_field('hours', 'option'); ?></p>
				<?php endif; ?>
			</div>
		</div>

		<div class="social">
			<a href="<?php the_field('facebook', 'option'); ?>" target="_blank">
				<svg>
					<use xlink:href="#facebook" />
				</svg>
			</a>
			<a href="<?php the_field('twitter', 'option'); ?>" target="_blank">
				<svg>
					<use xlink:href="#twitter" />
				</svg>
			</a>
			<a href="<?php the_field('linkedin', 'option'); ?>" target="_blank">
				<svg>
					<use xlink:href="#linkedin" />
				</svg>
			</a>
			<a href="<?php the_field('instagram', 'option'); ?>" target="_blank">
				<svg>
					<use xlink:href="#instagram" />
				</svg>
			</a>
			<a href="<?php the_field('youtube', 'option'); ?>" target="_blank">
				<svg>
					<use xlink:href="#youtube" />
				</svg>
			</a>
		</div>

		<div class="copyright">
			<div id="element5-credit" data-emergence="hidden">
				<a target="_blank" href="https://element5digital.com">
					<img src="https://element5digital.com/wp-content/themes/e5-starting-point/dist/images/element5_credit.svg" alt="Crafted by Element5 Digital" />
				</a>
			</div>
			<section class="legal">
				<p>© <?php echo date('Y'); ?> <?php echo get_bloginfo( 'name' ); ?>. All Rights Reserved.</p>
			</section>
		</div>

	</div>
</footer>