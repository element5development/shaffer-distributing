<?php 
/*----------------------------------------------------------------*\

	LOCATIONS SECTION

\*----------------------------------------------------------------*/
?>

<?php if( have_rows('location_repeater') ): ?>
<section class="locations">
	<h2><?php the_field('section_title'); ?></h2>
	<?php while ( have_rows('location_repeater') ) : the_row(); ?>
		<?php get_template_part( 'template-parts/elements/previews/preview-location' ); ?>
	<?php endwhile; ?>
</section>
<section class="location-maps">
	<?php while ( have_rows('location_repeater') ) : the_row(); ?>
		<div class="location-map"><?php the_sub_field('location_google_map'); ?></div>
	<?php endwhile; ?>
</section>
<?php endif; ?>