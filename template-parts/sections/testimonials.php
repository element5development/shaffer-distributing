<?php 
/*----------------------------------------------------------------*\

	TESTIMONIALS SECTION

\*----------------------------------------------------------------*/
?>

<section class="testimonials">
	<div class="block">
		<h2>Hear what our clients have to say</h2>
		<div class="testimonial-slider">
			<?php 
				$args = array(
					'post_type' => array('testimonial'),
					'posts_per_page' => -1,
					'ignore_sticky_posts' => true,
					'order' => 'DESC',
				);
				$testimonials = new WP_Query( $args );
			?>
			<?php if ( $testimonials->have_posts() ) { ?>
				<?php while ( $testimonials->have_posts() ) { $testimonials->the_post(); ?>
					<article class="testimonial">
						<blockquote>
							<p>"<?php the_field('testimonial'); ?>"</p>
							<h4><?php the_field('author'); ?></h4>
							<p><?php the_field('company'); ?></p>
						</blockquote>
					</article>
				<?php } ?>
			<?php } ?>
			<?php wp_reset_postdata(); ?>
		</div>
	</div>
</section>