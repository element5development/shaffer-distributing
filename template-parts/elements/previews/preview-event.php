<?php 
/*----------------------------------------------------------------*\

	PREVIEW ELEMENT FOR EVENTS

\*----------------------------------------------------------------*/
?>

<article class="preview preview-post">
	<div class="featured-image">
		<?php $image = get_sub_field('event_logo'); ?>
		<img src="<?php echo $image['sizes']['large']; ?>" alt="<?php echo $image['alt']; ?>" />
	</div>
	<div class="event-info">
		<p><?php the_sub_field('event_date'); ?></p>
		<h3><?php the_sub_field('event_title'); ?></h3>
		<p><?php the_sub_field('event_location'); ?></p>
	</div>
</article>