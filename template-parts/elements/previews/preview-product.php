<?php 
/*----------------------------------------------------------------*\

	PREVIEW ELEMENT FOR PRODUCTS

\*----------------------------------------------------------------*/
?>

<article class="preview preview-post preview-product">
	<a href="<?php the_permalink(); ?>"></a>
	<div class="featured-image">
		<?php $image = get_field('product_image'); ?>
		<img src="<?php echo $image['sizes']['small']; ?>" alt="<?php echo $image['alt']; ?>" />
	</div>
	<h4><?php the_field('price'); ?></h4>
	<h2><?php the_title(); ?></h2>
	<div class="buttons">
		<div class="button is-ghost">View Details</div>
	</div>
</article>