<?php 
/*----------------------------------------------------------------*\

	PREVIEW ELEMENT FOR MEMBER CARDS

\*----------------------------------------------------------------*/
?>

<article class="preview preview-post">
	<div class="featured-image">
		<?php $image = get_sub_field('headshot'); ?>
		<img src="<?php echo $image['sizes']['small']; ?>" alt="<?php echo $image['alt']; ?>" />
	</div>
	<div class="member-info">
		<h2><?php the_sub_field('title'); ?></h2>
		<p class="position"><?php the_sub_field('position'); ?></p>
		<p><?php the_sub_field('description'); ?></p>
		<?php $link = get_sub_field('button'); ?>
		<?php if( $link ): ?>
			<a class="button" href="<?php echo $link['url']; ?>" target="<?php echo $link['target']; ?>"><?php echo $link['title']; ?></a>
		<?php endif; ?>
	</div>
</article>