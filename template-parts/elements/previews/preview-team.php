<?php 
/*----------------------------------------------------------------*\

	PREVIEW ELEMENT FOR TEAM MEMBERS

\*----------------------------------------------------------------*/
?>

<article class="preview preview-team">
	<a href="mailto:<?php the_field('email'); ?>"></a>
	<div class="featured-image">
		<?php $image = get_field('headshot'); ?>
		<img src="<?php echo $image['sizes']['small']; ?>" alt="<?php echo $image['alt']; ?>" />
		<div class="overlay"></div>
		<div class="email-icon"></div>
	</div>
	<h2><?php the_title(); ?></h2>
	<p><?php the_field('position'); ?></p>
</article>