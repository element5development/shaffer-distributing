<?php 
/*----------------------------------------------------------------*\

	PREVIEW ELEMENT FOR FEATURED PRODUCTS

\*----------------------------------------------------------------*/
?>

<div>
	<div style="background-image: url('<?php the_sub_field( 'product_background'); ?>');"></div>
	<?php $product = get_sub_field('product'); ?>
	<?php if( $product ): ?>
		<?php foreach( $product as $product ) : ?>
			<article>
				<?php if ( get_field('product_image', $product->ID) ) : ?>
					<div class="featured-img rellax" data-rellax-speed="-2">
						<?php $image = get_field('product_image', $product->ID); ?>
						<img src="<?php echo $image['sizes']['large']; ?>" alt="<?php echo $image['alt']; ?>" />
					</div>
				<?php endif; ?>

				<div class="product-info rellax" data-rellax-speed="8">
					<div>
						<h2><?php echo get_the_title( $product->ID ); ?></h2>
						<?php if ( get_field('product_short_description', $product->ID) ) : ?>
							<p><?php the_field('product_short_description', $product->ID); ?></p>
						<?php elseif ( get_field('product_description', $product->ID) ) : ?>
							<p><?php the_field('product_description', $product->ID); ?></p>
						<?php endif; ?>
						<div class="buttons">
							<a class="button" href="<?php echo esc_url( get_permalink( $product->ID ) ); ?>">Learn More</a>
							<button class="is-transparent">Show</button>
						</div>
					</div>
					<div class="specifications">
						<h4>Specifications</h4>
						<?php if( have_rows('specifications', $product->ID) ): ?>
							<ul>
								<?php while ( have_rows('specifications', $product->ID) ) : the_row(); ?>
									<li><?php the_sub_field('specification', $product->ID); ?></li>
								<?php endwhile; ?>
							</ul>
						<?php endif; ?>
					</div>
				</div>
			</article>
		<?php endforeach; ?>
	<?php endif; ?>
</div>