<?php 
/*----------------------------------------------------------------*\

	PREVIEW ELEMENT FOR SEARCH RESULTS

\*----------------------------------------------------------------*/
?>

<article class="preview preview-blog">
	<a href="<?php the_permalink(); ?>"></a>
	<div class="blog-info">
		<?php echo get_the_term_list( $post->ID, 'category', '<a class="category">', ', ', '</a>' ) ?>
		<h3><?php the_title(); ?></h3>
		<p><?php echo get_excerpt(100); ?></p>
	</div>
</article>
