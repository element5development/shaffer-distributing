<?php 
/*----------------------------------------------------------------*\

	PREVIEW ELEMENT FOR LOCATIONS

\*----------------------------------------------------------------*/
?>

<article class="preview preview-post">
	<div class="featured-image">
		<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 80 80">
			<defs><style>.cls-icon-2{fill:none;stroke:#00c0cd;stroke-linecap:round;stroke-linejoin:round;stroke-width:2px}</style></defs>
			<path d="M40 2A38 38 0 1 1 2 40 38 38 0 0 1 40 2m0-2a40 40 0 1 0 40 40A40 40 0 0 0 40 0z" fill="#00c0cd" id="BG"/>
			<g id="Layer_1" data-name="Layer 1">
				<path class="cls-icon-2" d="M53.66 40.85L40 62.65 26.18 40.56a.05.05 0 0 1 0-.05 15.55 15.55 0 0 1-1.87-7.44 15.72 15.72 0 1 1 29.38 7.78z"/>
				<path class="cls-icon-2" d="M26.18 40.56v-.05a.05.05 0 0 0 0 .05z"/>
				<circle class="cls-icon-2" cx="40" cy="33.07" r="6.82"/>
			</g>
		</svg>
	</div>
	<div class="location-info">
		<h3><?php the_sub_field('location_name'); ?></h3>
		<p><?php the_sub_field('location_address'); ?><?php if ( get_sub_field('location_address_two') ) : ?><br><?php the_sub_field('location_address_two'); ?><?php endif; ?><br><?php the_sub_field('location_city_state_zip'); ?></p>
	</div>
</article>