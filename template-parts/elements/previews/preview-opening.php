<?php 
/*----------------------------------------------------------------*\

	PREVIEW ELEMENT FOR JOB OPENINGS

\*----------------------------------------------------------------*/
?>

<article class="preview preview-post">
	<div class="featured-image">
		<img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/careers-icon.svg" alt="Job Opening Icon" />
	</div>
	<div class="job-info">
		<h3><?php the_sub_field('opening_name'); ?></h3>
		<?php $link = get_sub_field('button'); ?>
		<?php if( $link ): ?>
			<a class="button" href="<?php echo $link['url']; ?>" target="<?php echo $link['target']; ?>"><?php echo $link['title']; ?></a>
		<?php endif; ?>
	</div>
</article>