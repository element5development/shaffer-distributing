<?php 
/*----------------------------------------------------------------*\

	TAXONOMY TEMPLATE

\*----------------------------------------------------------------*/
?>

<?php 
	//GET POST TYPE FROM PAGE
	//EMPTY FOR DEFAULT BLOG
	$my_query = new WP_Query( array( 
    'orderby' => 'date',
    'order' => get_query_var('order'), // will return order query string variable
	));
?>


<?php get_header(); ?>

<?php get_template_part('template-parts/elements/navigation'); ?>

<?php get_template_part('template-parts/elements/navigation-mobile'); ?>

<?php get_template_part('template-parts/sections/headers/header-taxonomy'); ?>

<main>
	<article> 
		<?php if ( have_posts() ) : ?>
			<?php if ( get_field( $posttype . '_editor', 'option') ) : ?>
				<section class="wysiwyg-block">
					<?php the_field( $posttype . '_editor', 'option'); ?>
				</section>
			<?php endif; ?>

			<div class="feed-with-sidebar">
				<section class="sidebar">
					<h4>Sort by</h4>
					<a href="?order=DESC" class="newest">Newest</a>
					<a href="?order=ASC" class="oldest">Oldest</a>
					<h4>Filter by</h4>
					<div class="select-wrap">
							<?php
								$termId = get_queried_object_id();
								$terms = get_terms( 'productcategory', 'parent='.$termId.'' );
	
								if($terms){
									$select = "<select name='cat' id='cat' class='postform'>";
									$select.= "<option value='-1'>By Category</option>";
								
									foreach($terms as $term){
										if($term->count > 0){
											$select.= "<option value='".$term->slug."'>".$term->name."</option>";
										}
									}
								
									$select.= "</select>";
								} else {
									$cat = get_queried_object();
									$parentID = $cat->parent;

									$terms = get_terms( 'productcategory', 'parent='.$parentID.'' );

									$select = "<select name='cat' id='cat' class='postform'>";
									$select.= "<option value='-1'>By Category</option>";
								
									foreach($terms as $term){
										if($term->count > 0){
											$select.= "<option value='".$term->slug."'>".$term->name."</option>";
										}
									}
								
									$select.= "</select>";
								}
								echo $select;
							?>
							<script type="text/javascript">
								var dropdown = document.getElementById("cat");

								function onCatChange() {
									if (dropdown.options[dropdown.selectedIndex].value != -1) {
										location.href = "<?php echo get_site_url(); ?>/product-category/" + dropdown.options[dropdown.selectedIndex].value + "/";
									}
								}
								dropdown.onchange = onCatChange;
							</script>
					</div>
					<div class="select-wrap">
							<?php
								$categories = get_categories('taxonomy=vendor');
	
								$select = "<select name='cat' id='vend' class='postform'>";
								$select.= "<option value='-1'>By Vendor</option>";
							
								foreach($categories as $category){
									if($category->count > 0){
											$select.= "<option value='".$category->slug."'>".$category->name."</option>";
									}
								}
							
								$select.= "</select>";
							
								echo $select;
							?>
							<script type="text/javascript">
								var dropdownv = document.getElementById("vend");

								function onCatChange() {
									if (dropdownv.options[dropdownv.selectedIndex].value != -1) {
										location.href = "<?php echo get_site_url(); ?>/vendor/" + dropdownv.options[dropdownv.selectedIndex].value + "/";
									}
								}
								dropdownv.onchange = onCatChange;
							</script>
					</div>
				</section>

				<section class="feed">
					<?php while ( have_posts() ) : the_post(); ?>
						<?php get_template_part('template-parts/elements/previews/preview-product'); ?>
					<?php endwhile; ?>
				</section>
				<section class="infinite-scroll is-standard-width has-small-spacing">
					<div class="page-load-status">
						<p class="infinite-scroll-request">
							<svg class="loading" x="0px" y="0px" width="40px" height="40px" viewBox="0 0 50 50" style="enable-background:new 0 0 50 50;">
								<path d="M43.935,25.145c0-10.318-8.364-18.683-18.683-18.683c-10.318,0-18.683,8.365-18.683,18.683h4.068c0-8.071,6.543-14.615,14.615-14.615c8.072,0,14.615,6.543,14.615,14.615H43.935z">
									<animateTransform attributeType="xml"
										attributeName="transform"
										type="rotate"
										from="0 25 25"
										to="360 25 25"
										dur="0.6s"
										repeatCount="indefinite"/>
								</path>
							</svg>
						</p>
						<p class="infinite-scroll-last"></p>
						<p class="infinite-scroll-error"></p>
					</div>
					<?php the_posts_pagination( array(
						'prev_text'	=> __( 'Previous page' ),
						'next_text'	=> __( 'Next page' ),
					) ); ?>
					<a class="load-more button">View more</a>
				</section>
			</div>
		<?php else : ?>
			<!-- NO RESULTS FOUND -->
		<?php endif; ?>
	</article>
</main>

<?php get_template_part('template-parts/sections/footers/footer'); ?>

<?php get_footer(); ?>