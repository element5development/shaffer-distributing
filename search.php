<?php 
/*----------------------------------------------------------------*\

	SEARCH RESULTS ARCHIVE

\*----------------------------------------------------------------*/
?>

<?php get_header(); ?>

<?php get_template_part('template-parts/elements/navigation'); ?>

<?php get_template_part('template-parts/elements/navigation-mobile'); ?>

<section class="search-header">
	<h2>Search Results for:</h2>
	<?php get_search_form(); ?>
</section>

<main>

	<article> 

		<section class="feed search-feed product-feed">
			<h2>Product Results</h2>
			<?php if ( have_posts() ) : ?>
				<?php while ( have_posts() ) : the_post(); ?>
					<?php if(get_post_type() == 'product'): ?>
						<?php get_template_part( 'template-parts/elements/previews/preview-product' ); ?>
					<?php endif; ?>
				<?php endwhile; ?>
			<?php else : ?>
				<h3>Sorry we couldn’t find any products related to what you searched for.</h3>
			<?php endif; ?>
		</section>

		<section class="feed feed-search">
			<h2>Other Results</h2>
			<?php if ( have_posts() ) : ?>
				<?php while ( have_posts() ) : the_post(); ?>
					<?php if(get_post_type() != 'product'): ?>
						<?php get_template_part('template-parts/elements/previews/preview-post'); ?>
					<?php endif; ?>
				<?php endwhile; ?>
			<?php else : ?>
				<h3>Sorry we couldn’t find anything related to what you searched for.</h3>
			<?php endif; ?>
		</section>
		<section class="infinite-scroll is-standard-width has-small-spacing">
			<div class="page-load-status">
				<p class="infinite-scroll-request">
					<svg class="loading" x="0px" y="0px" width="40px" height="40px" viewBox="0 0 50 50" style="enable-background:new 0 0 50 50;">
						<path d="M43.935,25.145c0-10.318-8.364-18.683-18.683-18.683c-10.318,0-18.683,8.365-18.683,18.683h4.068c0-8.071,6.543-14.615,14.615-14.615c8.072,0,14.615,6.543,14.615,14.615H43.935z">
							<animateTransform attributeType="xml"
								attributeName="transform"
								type="rotate"
								from="0 25 25"
								to="360 25 25"
								dur="0.6s"
								repeatCount="indefinite"/>
						</path>
					</svg>
				</p>
				<p class="infinite-scroll-last"></p>
				<p class="infinite-scroll-error"></p>
			</div>
			<?php the_posts_pagination( array(
				'prev_text'	=> __( 'Previous page' ),
				'next_text'	=> __( 'Next page' ),
			) ); ?>
			<a class="load-more button is-primary is-fluid">View more</a>
		</section>
	</article>

</main>

<?php get_template_part('template-parts/sections/footers/footer'); ?>

<?php get_footer(); ?>