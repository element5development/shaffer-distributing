<?php 
/*----------------------------------------------------------------*\

	DEFAULT SINGLE POST TEMPLATE
	More commonly only used for the default Blog/News post type.
	This is the page template for the post type, for the preview
	look under template-parts.

\*----------------------------------------------------------------*/
?>


<?php get_header(); ?>

<?php get_template_part('template-parts/elements/navigation'); ?>

<?php get_template_part('template-parts/elements/navigation-mobile'); ?>

<?php get_template_part('template-parts/sections/headers/header-post'); ?>

<main>

	<article> 
		<?php if ( '' !== get_post()->post_content ) : ?>
			<section class="wysiwyg-block">
				<?php the_content(); ?>
			</section>
		<?php endif; ?>
	</article>

</main>

<?php get_template_part('template-parts/sections/related'); ?>

<?php get_template_part('template-parts/sections/footers/footer'); ?>

<?php get_footer(); ?>