var $ = jQuery;

$(document).ready(function () {
	/*----------------------------------------------------------------*\
			STICKY AREAS
	\*----------------------------------------------------------------*/
	if ($(window).width() >= 800) {
		$('.sidebar').stickySidebar({
			topSpacing: 117,
			bottomSpacing: 0,
			stickyClass: 'is-sticky',
			containerSelector: '.feed-with-sidebar',
		});
		$('.team>div:first-of-type').stickySidebar({
			topSpacing: 117,
			bottomSpacing: 0,
			stickyClass: 'is-sticky',
			containerSelector: '.team>div:first-of-type',
		});
	}
});