var $ = jQuery;

$(document).ready(function () {
	$('a[href*=\\#]').bind('click', function (e) {
		e.preventDefault(); // prevent hard jump, the default behavior

		var target = $(this).attr("href"); // Set the target as variable

		// perform animated scrolling by getting top-position of target-element and set it as scroll target
		$('html, body').stop().animate({
			scrollTop: $(target).offset().top - 117
		}, 600, function () {});

		return false;
	});
});